## Formik

1. [React Form using Formik, Material-UI and Yup. - DEV Community](https://dev.to/finallynero/react-form-using-formik-material-ui-and-yup-2e8h)

2. [Simple React form validation with Formik, Yup and/or Spected](https://itnext.io/simple-react-form-validation-with-formik-yup-and-or-spected-206ebe9e7dcc)

3. [Formik — Handling files and reCaptcha – Hacker Noon](https://hackernoon.com/formik-handling-files-and-recaptcha-209cbeae10bc)

4. [The Joy of Forms with React and Formik | Keyhole Software](https://keyholesoftware.com/2017/10/23/the-joy-of-forms-with-react-and-formik/)

5. [Dynamic Forms with Formik in React with TypeScript](https://scottdj92.ghost.io/building-dynamic-forms-with-formik-with-react-and-typescript/)

6. [formik example](https://github.com/jaredpalmer/formik/blob/master/examples/Basic.tsx)

7. [formik example](https://github.com/jaredpalmer/formik/blob/master/examples/CombinedValidations.js)

8. [formik example](https://github.com/jaredpalmer/formik/blob/master/examples/ErrorMessage.js)

9. [jaredpalmer/formik-persist](https://github.com/jaredpalmer/formik-persist)

10. [React Formik + styled-components – Team Subchannel – Medium](https://medium.com/teamsubchannel/react-formik-styled-components-add78b37971f)

11.  [No more tears, handling Forms in React using Formik](https://dev.to/azure/no-more-tears-handling-forms-in-react-using-formik-part-i-20kp)

12. [React Formik Field Array](https://www.youtube.com/watch?v=Dm0TXbGvgvo&feature=share)

13. [formik/package.json at master · jaredpalmer/formik](https://github.com/jaredpalmer/formik/blob/master/package.json)

14. [formik-office-ui-fabric-react example - CodeSandbox](https://codesandbox.io/embed/oo7q88p06?codemirror=1)
15. [drop async validation](https://github.com/jaredpalmer/formik/issues/1524)