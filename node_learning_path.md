## basic
1. [type scripte](https://www.sitepen.com/blog/update-the-definitive-typescript-guide/)
    * [example repo](https://gitlab.com/nandigamchandu/learning/blob/master/src/30secondscode.ts)
2. [jest test module](https://jestjs.io/docs/en/getting-started)
    * [example repo](https://gitlab.com/nandigamchandu/learning/blob/master/src/30secondscode.test.js)
3. [share node load amoung clusters](https://nodejs.org/api/cluster.html)
4. [mongoose api](https://mongoosejs.com/docs/api.html#Aggregate)
5. [async](https://caolan.github.io/async/docs.html#)
6. [Handling authentication in Node using passport](http://www.passportjs.org/) => [sample project](https://medium.freecodecamp.org/learn-how-to-handle-authentication-with-node-using-passport-js-4a56ed18e81e) 
    * [repo](https://gitlab.com/nandigamchandu/professional_node/tree/master/auth_passport)
    * https://flaviocopes.com/express-post-query-variables/

## Asynchronous Programming
1.  [eloquentjavascript.net](https://eloquentjavascript.net/11_async.html)
2.  [what is promise](https://medium.com/javascript-scene/master-the-javascript-interview-what-is-a-promise-27fc71e77261)
3.  [Promises in depth](https://ponyfoo.com/articles/es6-promises-in-depth)
4.  

## Docker
1.  To access terminal on docker container. 
 ```bash
 docker exec -t -i <container-name>
 ```

## Postgres
1.  [Replicate your PostgreSQL database into another server using just one command](https://dev.to/fullstapps/replicate-your-postgresql-database-into-another-server-using-just-one-command-48km)
   ```bash
   pg_dump  -h   192.168.1.74 --username=venkat --password=Design_20 -p 5432 --dbname="usatravels" | psql -h localhost --username=postgres -p 5432 --dbname="usatravels"
   ```
2.  [How to enable remote access to PostgreSQL server on a Plesk server?](https://support.plesk.com/hc/en-us/articles/115003321434-How-to-enable-remote-access-to-PostgreSQL-server-on-a-Plesk-server-)