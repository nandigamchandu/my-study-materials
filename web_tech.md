## HTML and CSS

1. [HTML and CSS](https://www.safaribooksonline.com/library/view/html-css/9781118206911/) - chapters 3, 4, 6, 7, 8, 17
2. [Learning Web Design](https://www.amazon.com/Learning-Web-Design-Beginners-JavaScript/dp/1449319270)
3. [CSS In Depth](https://www.amazon.com/CSS-Depth-Keith-J-Grant/dp/1617293458/)
4. [A complete guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
5. [A complete guide to CSS Grid](https://css-tricks.com/snippets/css/complete-guide-grid/)
6. [CSS Grid Garden](http://cssgridgarden.com/)
7. [Flexbox Froggy](https://flexboxfroggy.com/)
8. [Flexbox Zombies](https://mastery.games/p/flexbox-zombies)
9. [Responsive Web Design with HTML5 and CSS3](https://www.amazon.com/Responsive-Web-Design-HTML5-CSS3/dp/1784398934)
10. [Building Websites With Bulma](https://www.pluralsight.com/courses/building-websites-bulma)
11. [Creating Interfaces With Bulma](https://www.oreilly.com/library/view/creating-interfaces-with/9781939902498/)

## Basic Programming

1. [Get Coding](https://www.amazon.com/Get-Coding-Learn-JavaScript-Website/dp/076369276X)
2. [Head First Javascript Programming](https://www.amazon.com/Head-First-JavaScript-Programming-Brain-Friendly/dp/144934013X)

## Javascript/Typescript

1. [The Principles of Object-Oriented JavaScript](https://www.amazon.com/Principles-Object-Oriented-JavaScript-Nicholas-Zakas/dp/1593275404)
2. [Javscript the good parts](https://www.safaribooksonline.com/library/view/javascript-the-good/9780596517748/) - chapters 3, 4, 6
3. [Eloquent Javscript](https://eloquentjavascript.net/)
4. [Exploring ES6](http://exploringjs.com/es6/) - chapters 4, 10, 13, 15
5. [Typescript Handbook](https://www.typescriptlang.org/docs/home.html)
6. [33 JS Concepts](https://github.com/leonardomso/33-js-concepts)
7. [30 Seconds Of Code](https://github.com/30-seconds/30-seconds-of-code)

## DOM

1.[Head first HTML5 programming](https://www.safaribooksonline.com/library/view/head-first-html5/9781449314712/) - chapters 2, 3, 6, 7, 9, 10

2. [Eloquent Javscript](https://eloquentjavascript.net/) - chapters 14, 15, 16

## Asynchrony

1. [Promises](https://medium.com/javascript-scene/master-the-javascript-interview-what-is-a-promise-27fc71e77261)
2. [axios](https://github.com/axios/axios)
3. [ky](https://github.com/sindresorhus/ky)
4. [Reactive programming with RXJS 5](https://www.safaribooksonline.com/library/view/reactive-programming-with/9781680505528/) - chapters 1, 2, 3

## React

1. [React for real](https://www.safaribooksonline.com/library/view/react-for-real/9781680502817/) - chapters 1, 2
2. [React: Rethinking Best Practices Talk](https://www.youtube.com/watch?v=x7cQ3mrcKaY)
3. [ReactJS - Main Concepts](https://reactjs.org/docs/hello-world.html)
4. [ReactJS Tutorial](https://reactjs.org/tutorial/tutorial.html)
5. [Learning React](https://www.safaribooksonline.com/library/view/learning-react-1st/9781491954614/) - chapters 3, 4, 5, 6

## State Management

1. [Immer](https://github.com/mweststrate/immer)
2. [Unstated](https://github.com/jamiebuilds/unstated)
3. [Reworm](https://github.com/pedronauck/reworm)
4. [Mobx](https://github.com/mobxjs/mobx)
5. [ReactJS Koans](https://github.com/arkency/reactjs_koans)

## Third Party Libraries

1.[React Virtualized](https://github.com/bvaughn/react-virtualized)

2. [localForage](https://github.com/localForage/localForage)
3. [rebass](https://rebassjs.org/)
4. [blueprint](https://blueprintjs.com/)
5. [material-ui](https://material-ui.com/)
6. [react-motion](https://github.com/chenglou/react-motion)
7. [react-router](https://github.com/ReactTraining/react-router)
8. [react-intl](https://github.com/yahoo/react-intl)
9. [react-dnd](https://github.com/react-dnd/react-dnd)
10.[emtion](https://emotion.sh/)
