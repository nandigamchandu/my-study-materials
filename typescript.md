## Type Script
1.  [Get started with typescript](https://www.robertcooper.me/get-started-with-typescript-in-2019)
2.  [Understanding TypeScript’s type notation](http://2ality.com/2018/04/type-notation-typescript.html)
3.  [the definitive typescript guide](https://www.sitepen.com/blog/update-the-definitive-typescript-guide/)
4.  [How to master advanced TypeScript patterns](https://medium.freecodecamp.org/typescript-curry-ramda-types-f747e99744ab)
5.  [Union types in TypeScript: modeling state](https://matiasklemola.com/typescript-union-types)
5.  [Programming TypeScript by Boris Cherny](https://learning.oreilly.com/library/view/programming-typescript/9781492037644/)-Text book
6.  [Hands-On Functional Programming with TypeScript By Remo H. Jansen](https://learning.oreilly.com/library/view/hands-on-functional-programming/9781788831437/)-Text - Book
7.  [typescript-utilities-cheatsheet](https://github.com/typescript-cheatsheets/typescript-utilities-cheatsheet)

### How to setup type script
#### setting up vscode
