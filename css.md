palmerhq/typescript: TypeScript coding guidelines & configs for The Palmer Group
https://github.com/palmerhq/typescript

Learn the Chrome Developer Tools to Debug and Optimize Sites and Web Apps
https://frontendmasters.com/courses/chrome-dev-tools-v2/

Building an Online Retail Dashboard in React ― Scotch.io
https://scotch.io/tutorials/building-an-online-retail-dashboard-in-react

Stress Testing React Easy State – Bertalan Miklos – Medium
https://medium.com/@solkimicreb/stress-testing-react-easy-state-ac321fa3becf

Data fetching in React the functional way powered by TypeScript, io-ts & fp-ts - DEV Community
https://dev.to/remojansen/data-fetching-in-react-the-functional-way-powered-by-typescript-io-ts--fp-ts-ojf

