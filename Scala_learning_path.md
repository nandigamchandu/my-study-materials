# scala text books
1. [scala documentation](https://docs.scala-lang.org/tour/tour-of-scala.html) 
2. [Json lib](https://alvinalexander.com/scala/how-to-create-json-strings-from-scala-objects)
    *   [Json4s](http://json4s.org/)
3. [add packages to RPEL](https://alvinalexander.com/scala/scala-how-add-jar-file-scala-repl-classpath-command-line)
4. [set up project](https://docs.scala-lang.org/getting-started/intellij-track/getting-started-with-scala-in-intellij.html)
5. 