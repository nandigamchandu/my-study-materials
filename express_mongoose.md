1. [express example](https://flaviocopes.com/express/)
2. [mongo database install](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)
3. [mongoose.js](https://mongoosejs.com/docs/guide.html)
4. [professional node js](https://github.com/thinkphp/nodejs-books/blob/master/public/Professional%20Node.js.pdf) => chapter 21
6. [compass installer](https://docs.mongodb.com/compass/master/install/)
7. eaxmple projects
     * [user manager](https://gitlab.com/nandigamchandu/professional_node/tree/master/usermanage_app)
     * [task manager](https://gitlab.com/nandigamchandu/professional_node/tree/master/user_manage_app)
8.  [pervez sir express starter](https://github.com/pervezfunctor/express-starter)
9.  [express project generator](https://expressjs.com/en/starter/generator.html)
10. [method overrider](https://github.com/expressjs/method-override)
11. [Learn how to handle authentication with Node using Passport.js](https://medium.freecodecamp.org/learn-how-to-handle-authentication-with-node-using-passport-js-4a56ed18e81e)
12. [body parser](https://github.com/expressjs/body-parser)
13. [express middlewares](https://expressjs.com/en/resources/middleware.html)