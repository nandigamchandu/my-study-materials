## JavaScript
[java script basics](https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript)

### JavaScript Exercise
1. [30 seconds code](https://30secondsofcode.org/)
2. [javascript](https://javascript.info/)

## TypeScript
1.  [Get started with typescript](https://www.robertcooper.me/get-started-with-typescript-in-2019)
2.  [Understanding TypeScript’s type notation](http://2ality.com/2018/04/type-notation-typescript.html)
3.  [the definitive typescript guide](https://www.sitepen.com/blog/update-the-definitive-typescript-guide/)
4.  [How to master advanced TypeScript patterns](https://medium.freecodecamp.org/typescript-curry-ramda-types-f747e99744ab)
5.  [Union types in TypeScript: modeling state](https://matiasklemola.com/typescript-union-types)
5.  [Programming TypeScript by Boris Cherny](https://learning.oreilly.com/library/view/programming-typescript/9781492037644/)-Text book
6.  [Hands-On Functional Programming with TypeScript By Remo H. Jansen](https://learning.oreilly.com/library/view/hands-on-functional-programming/9781788831437/)-Text - Book
7.  [typescript-utilities-cheatsheet](https://github.com/typescript-cheatsheets/typescript-utilities-cheatsheet)
8.  [TypeScript Deep Dive](https://basarat.gitbooks.io/typescript/)


## Test Modules
1.  [Jest](https://jestjs.io/docs/en/getting-started) Read upto mock functions

## CSS
1. [marksheet](https://marksheet.io/)
2. [Css tutorials](https://flaviocopes.com/tags/css/)
3. [CSS in js](https://medium.com/seek-blog/a-unified-styling-language-d0c208de2660)
4. [Grid Garden - A game for learning CSS grid](https://cssgridgarden.com/)
5. [A game for learning to flexibly align and wrap elements with CSS flexbox](https://codepip.com/games/flexbox-froggy/)
6. 

## React
1.  [learning React and programming the tic-tac-toe game](https://reactjs.org/tutorial/tutorial.html)
2.  [React 101 - A Practical Introduction](https://sebhastian.com/learning-the-basics-of-React-by-doing)
3.  [Framer ES6 / React Guide](https://paper.dropbox.com/doc/Framer-ES6-React-Guide-Th7joG9fFSSiyZgOFYqj6)
4.  [A React simple app example: fetch GitHub users information via API](https://flaviocopes.com/react-example-githubusers/)
5.  [React List Components by Example](https://www.robinwieruch.de/react-list-components/)
6.  [What are React Hooks?](https://www.robinwieruch.de/react-hooks/)
7.  [How to fetch data with React Hooks?](https://www.robinwieruch.de/react-hooks-fetch-data/)
8.  [React State with Hooks: useReducer, useState, useContext](https://www.robinwieruch.de/react-state-usereducer-usestate-usecontext/)
9.  [The Approachable Guide to useReducer](https://www.seangroff.com/useReducer/)
10. [react router](https://auth0.com/blog/react-router-4-practical-tutorial/)
11. [formik-resources](https://gitlab.com/nandigamchandu/my-study-materials/blob/master/formik.md)
12. [Learn React By Itself](https://reactarmory.com/guides/learn-react-by-itself)
13. [react-async](https://github.com/ghengeveld/react-async):React component and hook for declarative promise resolution and data fetching
14. [json-server](https://github.com/typicode/json-server):Get a full fake REST API with zero coding=>
15. [react-datepicker](https://github.com/Hacker0x01/react-datepicker)
16. [## react Resouses](https://reactresources.com/topics/getting-started)
17. [React-Hook-Form](https://react-hook-form.com/builder)
18. [what new in react 16.9](https://scotch.io/bar-talk/whats-new-in-react-169)
19. https://egghead.io/courses/the-beginner-s-guide-to-react
20. https://egghead.io/courses/advanced-react-component-patterns
21. https://egghead.io/courses/simplify-react-apps-with-react-hooks
22. https://mxstbr.blog/2017/02/react-children-deepdive/
23. https://egghead.io/playlists/react-hooks-and-suspense-650307f2
24. https://www.robinwieruch.de/react-hooks-fetch-data/
25. https://www.youtube.com/watch?v=6RhOzQciVwI&list=PL4cUxeGkcC9hNokByJilPg5g9m2APUePI
26. [data-fetching-in-react-the-functional-way-powered-by-typescript-io-ts--fp-ts](https://dev.to/remojansen/data-fetching-in-react-the-functional-way-powered-by-typescript-io-ts--fp-ts-ojf)
27. [the state reduce pattern with react hooks](https://kentcdodds.com/blog/the-state-reducer-pattern-with-react-hooks)
28. [headless user interface components](https://www.merrickchristensen.com/articles/headless-user-interface-components/)
29. [Updating state from props isn't needed very often](https://www.robinwieruch.de/react-derive-state-props)
30. [A (Mostly) Complete Guide to React Rendering Behavior](https://blog.isquaredsoftware.com/2020/05/blogged-answers-a-mostly-complete-guide-to-react-rendering-behavior/)
31. [Formik Fast Field](https://github.com/formik/formik/issues/342)
32. [5 awsome packages](https://medium.com/javascript-in-plain-english/5-awesome-react-packages-you-need-to-try-out-20a156d3d73e)
33. [React authentication](https://courses.reactsecurity.io/courses/react-security-fundamentals-downloadable)

## Interesting React Projests
1. [React error boundary](https://www.npmjs.com/package/react-error-boundary)
2. [react-libraries](https://www.robinwieruch.de/react-libraries)
3. [A List of Useful npm Packages for React Developers](https://dev.to/manindu/a-list-of-useful-npm-packages-for-react-developers-3dhg)
4. [awesome-react-components](https://github.com/brillout/awesome-react-components)

## React starter
1. [pervez sir react starter](https://github.com/pervezfunctor/react-starter)
2. [react starter](https://facebook.github.io/create-react-app/docs/getting-started)

## topics
1. [How to disable cros](https://alfilatov.com/posts/run-chrome-without-cors/)
Windows
```
"[PATH_TO_CHROME]\chrome.exe" --disable-web-security --disable-gpu --user-data-dir=~/chromeTemp
```
Example : 
```
"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --disable-web-security --disable-gpu --user-data-dir=~/chromeTemp
```
Linux
```
google-chrome --disable-web-security
```

If you need access to local files for dev purposes like AJAX or JSON, you can use -–allow-file-access-from-files flag.

## Crocs 
1.  [google-chrome --disable-web-security --user-data-dir="/tmp/chrome_tmp"](https://askubuntu.com/questions/1128405/google-chrome-in-launcher-got-replaced)

## Plugins
1.  [Fabulous introduces a CSS properties sidebar into Visual Studio Code](https://github.com/Raathigesh/fabulous)

## Git Material
1. [how to move remote repo](https://www.atlassian.com/git/tutorials/git-move-repository)

## Importent Web sites
1. [Regex](https://regexr.com/)
