# Java and Microservices Course.

## First Month

1. [Functional Programming in Java](https://www.amazon.com/Functional-Programming-Java-functional-techniques/dp/1617292737) - chapters 1 and 2 only.
2. [The Java Programming Language 4th Edition](https://www.amazon.com/Java-Programming-Language-4th/dp/0321349806) - chapters 4, 11 and 12 for interfaces, generics and exceptions
3. [Modern Java in Action](https://www.amazon.com/Modern-Java-Action-functional-programming/dp/1617293563) - Part 1, 2 and 5. This is the main book for Java. FP and Concurrency topics
4. [Effective Java](https://www.amazon.com/Effective-Java-Joshua-Bloch/dp/0134685997) - Every item is important
5. [Learning Java](https://www.amazon.com/Learning-Java-Introduction-Real-World-Programming/dp/1492056278) - Chapter 7 for exceptions
6. [Java Performance](https://www.amazon.com/Java-Performance-Depth-Advice-Programming/dp/1492056111) - chapters 2, 3 and 5 are essential
7. [Java Concurrency in Practice](https://www.amazon.com/Java-Concurrency-Practice-Brian-Goetz/dp/0321349601) - Skim through Part 1, don't need to understand every sentence.
8. [Head First Design Patterns](https://www.amazon.com/Head-First-Design-Patterns-Object-Oriented/dp/149207800X) - chapters 1, 2, 6, 9, 12 are essential. Whole book is recommended reading.
9. [Learning SQL](https://www.amazon.com/Learning-SQL-Generate-Manipulate-Retrieve/dp/1492057614) - chapters 3, 4, 5, 6, 8, 9 are essential. Whole book is highly recommended.
10. [Patterns of Distributed Systems](https://martinfowler.com/articles/patterns-of-distributed-systems/) - Everything is essential.
11. [Distributed Systems](https://www.youtube.com/watch?v=UEAMfLPZZhE&list=PLeKd45zvjcDFUEv_ohr_HdUFe97RItdiB) - Every video is essential.

## Second Month

1. [Microservices Patterns](https://www.amazon.com/Microservices-Patterns-examples-Chris-Richardson/dp/1617294543) - chapters 4, 6, and 7 are essential. Excellent reference.
2. [Spring in Action](https://www.amazon.com/Spring-Action-Craig-Walls/dp/1617294942) - Part 1 and 2 are essential. Part 3 is recommended.
3. [Spring Microservices in Action](https://www.amazon.com/Spring-Microservices-Action-John-Carnell/dp/1617293989) - chapters 2, 4, 5, 7 and 8 are essential. Excellent reference. As this a an old book, don't worry too much about the details, understand the ideas.
4. [MongoDB the Definitive Guide](https://www.amazon.com/MongoDB-Definitive-Powerful-Scalable-Storage/dp/1491954469) - chapters 2, 3, 4, 5, 7 are essential. Excellent reference
5. [Apache Pulsar in Action](https://www.manning.com/books/apache-pulsar-in-action) - chapters 1-5 and 8, 9 are essential.

## Third Month

Implement one of the following three liveProjects.

1. [Open Banking App](https://www.manning.com/liveproject/create-an-open-banking-app-using-openapis-and-spring-boot)
2. [eCommerce WebApp](https://www.manning.com/liveproject/building-an-e-commerce-web-application-with-spring-boot)
3. [Online Booking System](https://www.manning.com/liveproject/creating-an-online-booking-system-using-spring-boot-and-reactjs)

## Recommended Reading

1. [Designing Data Intensive Applications](https://www.amazon.com/Designing-Data-Intensive-Applications-Reliable-Maintainable/dp/1449373321)
2. [Database Internals(https://www.amazon.com/Database-Internals-Deep-Distributed-Systems/dp/1492040347)
3. [Domain Driven Design](https://www.amazon.com/Domain-Driven-Design-Tackling-Complexity-Software/dp/0321125215)
4. [Reactive Design Patterns](https://www.amazon.com/Reactive-Design-Patterns/dp/B07KRPH7DX)
5. [Stream Processing with Apache Flink](https://www.amazon.com/Stream-Processing-Apache-Flink-Implementation/dp/149197429X)
6. [Learning Spark](https://www.amazon.com/Learning-Spark-Jules-Damji/dp/1492050040)
7. [Kafka The Definitive Guide](https://www.amazon.com/Kafka-Definitive-Real-Time-Stream-Processing/dp/1491936169)
8. [Designing Event Driven Systems](https://learning.oreilly.com/library/view/designing-event-driven-systems/9781492038252/)
