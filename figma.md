## Figma

1. [Design System in Figma Design + Code](https://designcode.io/design-system-in-figma)

2. [Figma tutorial](https://www.youtube.com/playlist?list=PLXDU_eVOJTx6zk5MDarIs0asNoZqlRG23)

3. [How to design flexible Button Components in Figma](https://www.youtube.com/watch?v=9xBahCYnzlA&list=PLKFUh46KjXERWsJTTGT5rdqtD6XWmd726&index=2&t=0s)

4. [How to Work with SVGs in Figma, HTML, and CSS | Optimized SVG Icons](https://www.youtube.com/watch?v=R0oz8DsxeYU&list=PLKFUh46KjXERWsJTTGT5rdqtD6XWmd726&index=2)

5. [How to Design Complex Components in Figma | Card UI with Layout Grids](https://www.youtube.com/watch?v=TS87r64iAVE&list=PLKFUh46KjXERWsJTTGT5rdqtD6XWmd726&index=3)

6. [How to build an inbox UI](https://www.youtube.com/playlist?list=PLKFUh46KjXES8mh_heA1WxQd98Wpekfuh)

7. [Free UX Flowchart kit for Sketch/Figma](https://uxflow.pro/)

8. [Figma Tutorial - A Free UI Design/Prototyping Tool. It's awesome.](https://www.youtube.com/watch?v=3q3FV65ZrUs)

9. [The Beginner's Guide to Figma from @jsjoeio on @eggheadio](https://egghead.io/courses/the-beginner-s-guide-to-figma)


## Design systems

1. [Building design systems](https://learning.oreilly.com/library/view/building-design-systems/9781484245149/)

2.  [Design Systems with React and Typescript in Storybook ](https://egghead.io/courses/design-systems-with-react-and-typescript-in-storybook)

3. [Priceline One Design system](https://pricelinelabs.github.io/design-system/)

4.  [Lightning Design System](https://www.lightningdesignsystem.com/)

5. [Component Based Design System With Styled-System](https://varun.ca/styled-system/)

6. [The component library behind the Carbon Design System](https://github.com/IBM/carbon-components)

7. [Carbon Design System](https://next.carbondesignsystem.com/)

8. [Buttons overview - Australian Government Design System](https://designsystem.gov.au/components/buttons/)