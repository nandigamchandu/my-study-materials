## React
1. [React tutorial tic tok with undo option](https://reactjs.org/tutorial/tutorial.html)
    *   [tic tac toe my gir repo](https://gitlab.com/nandigamchandu/react_tic_tac_toe)
2. [Top 3 React articles of the week in your inbox](https://tinyreact.email/2019-05-21.html)
3. [What are React Hooks?](https://www.robinwieruch.de/react-hooks/)
4. [How to fetch data with React Hooks?](https://www.robinwieruch.de/react-hooks-fetch-data/)
5. [React State with Hooks: useReducer, useState, useContext](https://www.robinwieruch.de/react-state-usereducer-usestate-usecontext/)

## CSS
### Important topics are box model, flexbox, css grid, animations and spacing
1.  [CSS and html tutorial](https://marksheet.io/)
2.  [CSS](https://flaviocopes.com/tags/css/)
3.  [Bulma based on flexbox](https://bulma.io/)
    *   [Create interface with Bulma text book](https://learning.oreilly.com/library/view/creating-interfaces-with/9781939902498/)
    *   [jmaczan/bulma-helpers: Library with missing Functional / Atomic CSS classes for Bulma framework](https://github.com/jmaczan/bulma-helpers)
4.  [Sass Basics](https://sass-lang.com/guide)
    *   [Sass syntax](https://sass-lang.com/documentation/syntax)
5.  [learn how these themes especially litera is created using bulma sass variables and overrides.](https://jenil.github.io/bulmaswatch/)