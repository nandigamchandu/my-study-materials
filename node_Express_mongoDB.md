## Express
1.  [simple start](https://flaviocopes.com/express/)
2.  [Pervez sir express starter](https://github.com/pervezfunctor/express-starter)
3.  [Express application generator](https://expressjs.com/en/starter/generator.html)
4.  [Express typescript generator](https://www.npmjs.com/package/express-generator-typescript)
5.  [Learn how to handle authentication with Node using Passport](https://medium.com/signature-networks/learn-how-to-handle-authentication-with-node-using-passport-js-4a56ed18e81e)
    *   [passport.js](http://www.passportjs.org/)
6.  [express middlewares](https://expressjs.com/en/resources/middleware.html)

## Nest
1.  [Backend APIs with Nest.js](https://auth0.com/blog/full-stack-typescript-apps-part-1-developing-backend-apis-with-nestjs/)


## Node
1.  [Professional Node text book](https://github.com/thinkphp/nodejs-books/blob/master/public/Professional%20Node.js.pdf)
    *   sample project for express => Read chapter-21 
    *   sample project for express-mongoose => Read chapter-25
2.  [Learn Node!](https://learnnode.com/)


## Test module
1. [supertest](https://www.npmjs.com/package/supertest)

## mongoDB
1.  [Mongoose.js](https://mongoosejs.com/docs/guide.html)
2.  [To Install mongodb](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)
3.  [To Install MongoDB client](https://docs.mongodb.com/compass/master/install/)
4.  [Typegoose - Define Mongoose models using TypeScript classes](https://github.com/szokodiakos/typegoose)
5.  [Slow Trains in MongoDB and Node.js](http://thecodebarbarian.com/slow-trains-in-mongodb-and-nodejs.html#break-up-one-slow-operation-into-many-fast-operations)


